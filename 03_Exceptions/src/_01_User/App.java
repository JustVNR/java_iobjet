package _01_User;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * T�l�charger : apache-log4j-2.19.0-bin.zip
 * 
 * ici : https://www.apache.org/dyn/closer.lua/logging/log4j/2.19.0/apache-log4j-2.19.0-bin.zip
 * 
 * Extraire le .zip et r�cup�rer :
 * - log4j-api-2.19.0.jar
 * - log4j-core-2.19.0.jar
 * 
 * Cr�er un dossier lib et y mettre les 2 .jar
 * Ajouter les librairies au "build path" clic droit => "Build Path" => "Conficure Build Path" => Onglet "librairies" => Add jars
 * 
 * Pour cr�er une configuration perso : 
 * 
 * Cr�er un dossier "ressources"
 * Dans le dossier "ressources" cr�er fichier log4j2.xml
 * R�cup�rer le contenu du fichier xml ici : https://logging.apache.org/log4j/2.x/manual/configuration.html
 * 
 */

public class App {

	private static Logger logger = LogManager.getLogger(App.class);
	
	public static void main(String[] args) {

		// Le bloc "try" permet d'imp�menter des instructions susceptibles de lever des
		// exceptions
		
		try {

			User user = new User("Duck", "Riri", 12);

			System.out.println(user);

			user.setAge(13);

			System.out.println(user);

			user.setAge(-12);
		} 
		catch (IllegalAgeException e) {

			//System.out.println(e.getMessage());
			logger.warn(e.getMessage());
		}
		catch (IllegalArgumentException e) {

			//System.out.println(e.getMessage());
			logger.warn(e.getMessage());
		}
		catch (Exception e) {

			//System.out.println(e.getMessage());
			logger.fatal(e.getMessage());
		}
		finally
		{
			System.out.println("Le bloc finally est ex�cut� quoiqu'il arrive. Avec ou sans exception.");
			System.out.println("Il est souvent utilis� pour lib�rer des ressources. (flux, cuonnexion � une BDD...");
		}
	}
}
