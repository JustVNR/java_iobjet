package _02_Parking;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

	private static Logger logger = LogManager.getLogger(App.class);
	
	public static void main(String[] args) {
		
		logger.info("Coucou je suis dans le package parking");

		Parking parc = new Parking(5);
		
		try {
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places disponibles");
			
		} catch (ParkingPleinException ex) {
			
			// ex.printStackTrace();
			// System.out.println(ex.getMessage());
			
			logger.error(ex.getMessage());
		}
	}
}
