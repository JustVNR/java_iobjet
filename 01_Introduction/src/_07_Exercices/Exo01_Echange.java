package _07_Exercices;

public class Exo01_Echange {

	//Permuter 2 variables

	/*
	 * Exemple :
	 * Donn�es d'entr�e : a = 5, b = 9
	 * Sortie pr�vue :
	 * Avant l��change : a = 5, b = 9
	 * Apr�s l��change : a = 9, b = 5
	 */
	
	public static void main(String[] args) {
		
		int a = 5;
		int b = 9;
		
		System.out.println("a = " + a + ", b = " + b);
		
		int temp = a;
		
		a = b;
		
		b = temp;
		
		System.out.println("a = " + a + ", b = " + b);
	}
}
