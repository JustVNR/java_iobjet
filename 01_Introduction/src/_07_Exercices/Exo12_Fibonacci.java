package _07_Exercices;

public class Exo12_Fibonacci {

	/*
	 * 
	 * En math�matiques, la suite de Fibonacci est une suite d'entiers dans laquelle chaque terme est la somme des deux termes qui le pr�c�dent.
	 * Elle commence par les termes 0 et 1 si on part de l'indice 0, ou par 1 et 1 si on part de l'indice 1.
	 * Dans les exercices suivants, nous nous concentrerons sur des tableaux � une dimension. Nous vous montrerons comment impl�menter et utiliser des tableaux.
	 * 
	 * Exemple : 1 1 2 3 5 8 13 21 34�.
	 * 
	 * Not�e (Fn), elle est donc d�finie par :
	 * 
	 * F0 = 0,
	 * F1 = 1 et
	 * Fn = Fn-1 + Fn-2 pour n >= 2
	 * 
	 * On peut d�finir cette suite par r�currence.
	 * 
	 * On initialise les formules avec deux conditions initiales : F0 = 0, F1 = 1
	 * Formule de r�currence : Fn = Fn-1 + Fn-2 pour n >= 2
	 */
	
	public static void main(String[] args) {
		
	}
}
