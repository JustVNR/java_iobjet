package _07_Exercices;

import java.util.Scanner;

public class Exo02_RacineCarree {

	// �crire un programme qui demande � l'utilisateur de rentrer un nombre entier et qui retourne sa racine carr�e.

	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in)
				;
		System.out.println("Entrer un entier");
		
		int n = clavier.nextInt();
		
		System.out.println("La racine carr�e de " + n + " vaut : " + (int)Math.sqrt(n));
		
		clavier.close();
	}
}
