package _04_Operateurs;

public class _04_Logiques {
	
	public static void main(String[] args) {

		/*
		 * Les op�rateurs logiques s'appliquent � des op�randes de type bool�en et produisent un r�sultat �galemnt bool�en
		 * 
		 * Java comporte 3 op�rateurs logiques (dont un unaire)
		 * 
		 * - ET : A && B => true si A ET B sont vrais, flase sinon
		 * - OU : A || B => true si A OU B est vrai, false sinon
		 * - NON : !A => true si A est false, false si A est true
		 */
		
		boolean A = false;
		boolean B = true;
		
		if (A && B) {
			System.out.println("A et B sont vrais");
		}
		else
		{
			System.out.println("A et B ne sont tous les 2 vrais");
		}
		
		if (A || B) {
			System.out.println("A ou B est vrai");
		}
		
		if (!A) {
			System.out.println("A n'est pas vrai");
		}
	}
}
