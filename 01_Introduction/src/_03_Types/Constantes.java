package _03_Types;

public class Constantes {

	public static void main(String[] args) {

		/*
		 * Une constante est identifi� par le mot cl� "final"
		 * Par convention les constantes sont d�clar�es en majuscules
		 */
		
		final String MA_CONSTANTE = "Cette chaine de caract�res est une constante.";
		
		// MA_CONSTANTE = "Cette chaine de caract�res est une autre constante."; //Interdit de modifier une constante
		
		System.out.println(MA_CONSTANTE);
		
		final int VIES_MAX = 15;
		
		System.out.println(VIES_MAX);
	}
}
