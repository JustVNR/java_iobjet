package _03_Types;

public class ReglesDeNommage {

	public static void main(String[] args) {

		/*
		 * Une variable peut commencer au choix par :
		 * - une lettre
		 * - un "_"
		 * - le caract�re "$"
		 * 
		 * mais par un chiffre ou un op�rateur
		 */
		
		int monEntier = 10;
		
		String _maChaineDeCaractere = "ma chaine de caract�res";
		
		char $monCaractere = 'a';
		
		//double 00monDouble; Innterdit
		// double +monDouble ; Interdit
		
		/*
		 * Par convention, on utilise le camelCase pour les variables
		 * 
		 * - Camel case : camelCase
		 * - Pascal case : PascalCase
		 * - Snake Case : snake_case
		 */
	}

}
