package _03_Types._02_Types_complexes;

/*
 * Un type �num�r� est un type de donn�es comportant un ensemble fini d'�tats (ou de valeurs) auxquels sont associ�s un nom;
 * Un type �nu�r� doit �tre d�clar� daun un fichier s�par� portant le nom de l'enum
 * Un type �num�r� s'introduit avec lemot cl� "enum"
 * Conventionnelement : 
 * - le nom du type �num�r� commence par une par une majuscule (PascalCase) 
 * - les noms d'�tats sont en MAJUSCULES
 * - Utilisation du "_" pour lesmots compos�s
 */

public enum MonEnum {
	VERT, ORANGE, ROUGE, ROUGE_FONCE
}
