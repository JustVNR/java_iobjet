package _03_Types._02_Types_complexes;


/*
 * Contrairement aux types primitifs, les types complexes (r�f�rences, objets) exposent des m�thodes;
 * Ils se distinguent visuellement des types primitifs en ce qu'ils commencent par une majuscule (PascalCase)
 * 
 * Les variables bas�es sur des types "objets" sont des pointeurs r�f�ren�ant les addresses m�moires desdits objets.
 * Leur valeur par d�faut est "null".
 */

public class _01_Strings {

	public static void main(String[] args) {
		
		String firstName = "riri";
		String lastName = "duck";
		
		String fullName = firstName + lastName; // Concat�nation de chaines de caract�res via l'op�rateur "+"
		
		/*
		 * La classe String est impl�ment�e de mani�re � produire des objets immuables (immutables)
		 * => Une fois produite, le contenu de la chaine de caract�res n'est plus modifiable.
		 * De nombreuses m�thodes permettent n�anmoins de produire de nouvelles chaines "modifi�es"  partir d'une chaine originale.
		 * Ces m�thodes retournent une chaine de caract�res qui pourra �tre r�allou�e � la chaine originale.
		 */
		
		System.out.println(firstName);
		firstName = firstName.toUpperCase();
		System.out.println(firstName);
		
		firstName = firstName.toLowerCase();
		
		String syllabe ="ri";
		
		String joinded = syllabe + syllabe;
		
		if (firstName == joinded) {
			System.out.println("Les 2 chaines pointent ver la m�me adresse"); // Non, "firstName" et "joinded" pointent vers des adresses diff�rentes
		}
		else
		{
			System.out.println("Les 2 chaines pointent vers des adresses diff�rentes.");
		}
		
		if (firstName.equals(joinded)) {
			System.out.println("Les 2 chaines ont des contenus identiques");
		}
		
		String s1 = "Bonjour";
		String s2 = "Bonjour"; // N'alloue pas d'autre m�moire car une variable ayant la m�me valeur existe d�j�
		
		String s3 = new String("Bonjour"); // Alloue de la m�moire car appel au "Constructeur" (new) de la classe String 
		
		System.out.println(s1 == s2); // TRUE
		System.out.println(s1 == s3); // FALSE
		
		System.out.println(s1.equals(s3)); // TRUE
		
		System.out.println(s1.charAt(2)); // 'n' retourne le caraact�re d'indice 2 (3i�me caract�re de "Bonjour"...)
		
		System.out.println(s1.concat(" Riri"));
		
		System.out.println(s1.substring(2)); // njour Retourne la sous chaine � partir de l'indice 2
		System.out.println(s1.substring(2, 4)); // nj Retourne la sous chaine entre indice min et indice max
		
		System.out.println(s1.toUpperCase()); // BONJOUR
		System.out.println(s1.toLowerCase()); // bonjour
		
		s1 = "Ceci est une chaine de caract�res.";
		
		String[] splitted = s1.split(" "); // Retourne un tableau de chaines de caract�res
		
		for (String item : splitted) {
			System.out.print(item + " - ");
		}
		
		String replacement = s1.replace("Ceci", "Ce truc");
		
		System.out.println();
		System.out.println(replacement);
		
		
		/*
		 * Conversion de types primitifs en chaines de caract�res
		 * 
		 * Il est possible de convertir des valeurs num�riques (int, long...) en chaines de caract�res, et r�ciproquement.
		 * 
		 * Pour ce faire, il faut faire appel � la classe Englobante du type primitif consid�r�. (par exemple Integer pour int)
		 */
		
		String myStr = "123";
		int myInt = Integer.parseInt(myStr);
		
		myStr = "3.141519";
		
		double myDouble = Double.parseDouble(myStr);
		
		System.out.println(myInt + " - " + myDouble); // reconvertit implicitement les nombres en chaines de caract�res
		
		System.out.println("6" + 4 + 5); // 645
		System.out.println(4 + 5 + "7"); // 97
		System.out.println("6" + 4 * 5); // 620
		// System.out.println("6" + 4 - 5); // Erreur
		System.out.println("6" + (5 - 4)); //61
		System.out.println("6" + (4 - 5)); //6-1
		
		/*
		 * Contrairement � la classe String, la classe Stringbuilder permet de cr�er des chaines de caract�res modifiables.
		 * Il n'est donc pas necessaire de r�alouer de la m�moire � chaque fois qu'on effecrue une modification.
		 */
		
		StringBuilder builder = new StringBuilder("Ceci est une chaine de caract�res cr��e avec un StringBuilder. ");
		
		System.out.println(builder);
		
		builder.append("Il est donc possible de la modifier"); // pas besoin de r�affecter le r�sulat de .append() � la variable "builder" pour qu'elle soit modifi�e
		
		System.out.println(builder);
	}
}
