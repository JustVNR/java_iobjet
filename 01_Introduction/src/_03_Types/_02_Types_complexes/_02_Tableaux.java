package _03_Types._02_Types_complexes;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Un tableau est de type complexe (r�f�rence/objet)
 * Une fois initialis�, il n'est plus possible de changer sa taille.
 * Mais comme pour la classe "String", il est n�anmoins possible de r�affecter un nouveau tableau � la place de l'ancien
 */
public class _02_Tableaux {

	public static void main(String[] args) {

		int[] tab; // d�claration d'un tableau d'eniters non initialis�

		String[] names = { "riri", "fifi" }; // D�claration et initialisation d'un tableau de chaines de caract�res

		System.out.println(names.length); // 2

		System.out.println(names[1]); // fifi

		System.out.println("Parcours du tableau avec une boucle \"for\" traditionnelle");

		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}

		System.out.println("Parcours du tableau avec une boucle \"foreach\"");

		for (String name : names) {
			System.out.println(name);
		}

		/*
		 * Tableaux de tableaux
		 */

		String[][] names2D = { { "riri", "fifi", "loulou" }, { "donald", "daisy" } };

		for (int line = 0; line < names2D.length; line++) {
			for (int column = 0; column < names2D[line].length; column++) {
				System.out.print(names2D[line][column] + " ");
			}
			System.out.println(); // pour revenir � la ligne entre chaque "line"
		}

		/*
		 * ArrayList
		 * 
		 * Un ArrayList est un objet de type "Collection". Comme un tableau, une
		 * Collection permet de stocker un ensemble de valeurs. Mais contrairement � un
		 * tableau, une collection a une taille dynamique.
		 */

		ArrayList<String> namesCollection = new ArrayList<String>(); // Collection instanci�e sans pr�ciser de taille

		namesCollection.add("riri");
		namesCollection.add("fifi");
		namesCollection.add("loulou");

		System.out.println("Size = " + namesCollection.size());

		System.out.println("\n___________________ Arrays : Classe Utilitaire pour les tableaux ____________________\n");

		String[] tabStr = new String[5];
		Arrays.fill(tabStr, "Bonjour"); // => initialiser un tableau avec une valeur

		// System.out.println(tabStr);
		System.out.println(Arrays.toString(tabStr)); // toString => afficher un tableau sans avoir � faire de boucle

		int tab1[] = { 1, 5, 7, 3, 8 };
		int tab2[] = { 1, 5, 7, 3, 8 };

		// equals => comparaison de 2 tableaux
		System.out.println(Arrays.equals(tab1, tab2)); // true

		// sort => tri d'un tableau
		Arrays.sort(tab2);

		System.out.println(Arrays.toString(tab2));

		// binarySearch => recherche d'un �l�ment dans un tableau tri�
		System.out.println(Arrays.binarySearch(tab2, 5)); // retourne 2 car en 3i�me position

		System.out.println(Arrays.binarySearch(tab2, 6)); // retourne une valeur n�gative car n'apparait pas dans la
															// tableau
	}
}
