package _03_Types._01_Types_primitifs;

public class _04_Caracteres {

	public static void main(String[] args) {
		
		/*
		 * Un caract�re est stock� en m�moir sous forme d'une valeur num�rique cod�e sur 2 octets.
		 */
		
		char c = 'a';
		
		System.out.println(c);
		
		c = 97; // le code 97 correpond au caract�re 'a'
		
		System.out.println(c);
		
		// Caract�res sp�ciaux
		
		char c1 = '\n'; // retour � la ligne
		char c2 = '\t'; // tabulation
		char c3 = '\\'; // antislash
		char c4 = '\''; // apostrophe
		
		// M�thodes permettant de tester un caraact�re
		
		boolean isDigit = Character.isDigit('a'); 
		
		System.out.println(isDigit); // false
		
		boolean isLetter = Character.isLetter('a');
		
		System.out.println(isLetter); // true
		
		boolean isUpperCase = Character.isUpperCase('a');
		
		System.out.println(isUpperCase); // false
	}
}
