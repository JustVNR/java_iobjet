package _03_Types._01_Types_primitifs;

public class _01_Boolean {

	public static void main(String[] args) {
		
		/*
		 * Le type boolean d�finit 2 �tats possibles
		 * - vrai (true)
		 * - faux (false)
		 */

		boolean isTrue = true;
		boolean isFalse = false;
		
	}

}
