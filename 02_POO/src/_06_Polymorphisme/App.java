package _06_Polymorphisme;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {

		Animal animal = new Animal();
		
		animal.communiquer();
		
		List<Animal> animaux = new ArrayList<Animal>();
		
		animaux.add(new Chien()); // On peut ajouter des chiens 
		animaux.add(new Chat());  // ET des chats
		animaux.add(new Chat());  // dans une liste (ou un tableau) fortement typ� d'animaux
		animaux.add(new Chien());
		animaux.add(new Animal());
		
		for (Animal anim : animaux) {
			anim.communiquer(); // 2 animaux peuvent avoir des comportements diff�rents => polymorphisme
		}
	}

}
