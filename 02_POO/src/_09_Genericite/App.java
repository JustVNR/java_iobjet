package _09_Genericite;

public class App {

	public static void main(String[] args) {

		MyGeneric<String> myGenericStr = new MyGeneric<>("Hello");
		MyGeneric<Integer> myGenericInt = new MyGeneric<>(12);
		
		System.out.println(myGenericStr);
		System.out.println(myGenericInt);
		
		MyGeneric<String> myGenericStr2 = new MyGeneric<>("Hello World");
		
		System.out.println(myGenericStr.myEquals(myGenericStr2));
		
		System.out.println(myGenericInt.myEquals(new MyGeneric<>(13)));
	}
}
