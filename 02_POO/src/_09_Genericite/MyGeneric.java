package _09_Genericite;


/*
 * Le concept de g�n�ricit� peut s'appliquer � des classes, des m�thodes et des interfaces :
 * - identiques sur le plan algorithmique
 * - mais mainupant des types de donn�es diff�rents
 * 
 * Int�r�t : 
 * - Optimisation du code
 * - Moins de cast (transtypage) � faire
 * - Moins de risques d'erreur � l'ex�cution
 */

public class MyGeneric<T> {

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public MyGeneric(T data) {
		super();
		this.data = data;
	}

	@Override
	public String toString() {
		return "MyGeneric [data=" + data + "]";
	}
	
	// M�thode g�n�rique
	
	boolean myEquals(MyGeneric<T> other)
	{
		return data.equals(other.data);
	}
}
