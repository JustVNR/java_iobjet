package _07_Interfaces.Task2;

public class App {

	public static void main(String[] args) {

		/*
		 * On peut choisir p�armi plusieurs impl�mentations de l'interface "ProductRepository"
		 * 
		 * Dans tous les cas on aura acc�s � l'ensemble des m�thodes d�finies dans l'interface.
		 * 
		 * On pourra donc les utiliser sans avoir � modifier notre code lorsque l'on change d'impl�mentation (passage de SqlServer � MySql)
		 */
		ProductRepository _repo = new ProductMySqlRepositoryImpl();

		//ProductRepository _repo2 = new ProductSqlServerRepositoryImpl();
	}
}
