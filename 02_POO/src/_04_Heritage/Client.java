package _04_Heritage;

public class Client extends User{ // la classe "Client" h�rite de la classe "User"

	private String phone;
	
	private boolean subscriber;

	//Getters and Setters
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isSubscriber() {
		return subscriber;
	}

	public void setSubscriber(boolean subscriber) {
		this.subscriber = subscriber;
	}

	
	//Constructeurs
	public Client(boolean subscriber) {
		super();
		this.subscriber = subscriber;
	}

	public Client(String nom, String prenom, int age) {
		super(nom, prenom, age); // Appel au constructeur � 3 param�tres de la classe M�re (User)
		
//		super.nom = nom;
//		super.prenom = prenom;
//		super.age = age;
	}

	public Client(String nom, String prenom, int age, String phone) {
		this(nom, prenom, age); // Appel au constructeur � 3 param�tres de la classe Client
		
		this.setPhone(phone);
	}

	@Override
	public String toString() {
		return super.toString() + " Il/elle est client(e). Son num�ro est " + this.getPhone();
	}
}
